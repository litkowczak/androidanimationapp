package com.example.animationapp;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener{


    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    private ImageView imageView;
    private ImageView detailIV;
    private ImageView blingIV;
    private ConstraintLayout detail;
    private TextView detailtxt;
    private Button viewBTN;

    boolean scrolling;

    Runnable task1;
    Runnable task2;

    private int actualFrame=0;
    private int maxFrames = 180;
    private String actualName = "";

    private CountDownTimer timer;
    private CountDownTimer timer2;



    private Bitmap bm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.mainImageView);
        imageView.setImageResource(R.drawable.frame0000);
//        imageView.setDrawingCacheEnabled(true);
//        bm = imageView.getDrawingCache();

        blingIV = findViewById(R.id.blinkIV);
        blingIV.setImageResource(R.drawable.blink0000);
        imageView.setDrawingCacheEnabled(true);
        bm = imageView.getDrawingCache();

        setuptv();
        settouch();
        mDetector = new GestureDetectorCompat(this,this);
        mDetector.setOnDoubleTapListener(this);



//        timer = new CountDownTimer(500, 20) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                try{
//                    makeInvisible();
//                }catch(Exception e){
//                    Log.e("Error", "Error: " + e.toString());
//                }
//            }
//        }.start();
//
//        timer2 = new CountDownTimer(1500, 20) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//
//            }
//
//            @Override
//            public void onFinish() {
//                try{
//                    makeVisable();
//                }catch(Exception e){
//                    Log.e("Error", "Error: " + e.toString());
//                }
//            }
//        }.start();

        final Handler handler = new Handler();
        task1 = new Runnable() {
            @Override
            public void run() {
                if (!scrolling) {
                    makeVisable();
                }
                handler.postDelayed(task2, 700);

            }
        };
        task2 = new Runnable() {
            @Override
            public void run() {
                makeInvisible();
                handler.postDelayed(task1, 2000);


            }
        };
        handler.post(task1);


    }

    private void makeInvisible(){
        blingIV.setVisibility(View.INVISIBLE);
        //timer2.start();
    }

    private void makeVisable(){
        blingIV.setVisibility(View.VISIBLE);
        //timer.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG,"onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + distanceX+ " dist y : " + distanceY);
         String imgName = "frame0";
         String pngImgName = "blink0";

         scrolling = true;

        int dist;

        if(distanceX >=0){
            dist = (int) distanceX;
            dist = dist ==0 ? 1 : dist;
            if (dist > 3) dist = 3;
        }
        else {
            dist = (int) distanceX;
            dist = dist ==0 ? -1 : dist;
            if(dist < -3) dist = -3;
        }

        actualFrame += dist;

        if(actualFrame>maxFrames){
            actualFrame =maxFrames;
        }
        else if(actualFrame<0){
            actualFrame = 0;
        }

        if(actualFrame <10){
            imgName = imgName + "00"+actualFrame;
            pngImgName = pngImgName+"00"+actualFrame;
        }
        else if (actualFrame >=10 && actualFrame <100){
            imgName = imgName+"0"+actualFrame;
            pngImgName = pngImgName+"0"+actualFrame;
        }
        else {
            imgName = imgName+actualFrame;
            pngImgName = pngImgName+actualFrame;
        }

        actualName = imgName;

        int pngImage = getResources().getIdentifier(pngImgName,"drawable",getPackageName());
        blingIV.setImageResource(pngImage);

        int resImage = getResources().getIdentifier(imgName , "drawable", getPackageName());
        imageView.setImageResource(resImage);

        blingIV.setDrawingCacheEnabled(true);
        bm = blingIV.getDrawingCache();


        scrolling = false;
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {


        float eventX = event.getX();
        float eventY = event.getY();

//        imageView.setDrawingCacheEnabled(true);
//        //Bitmap bm = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
//        Bitmap bm = imageView.getDrawingCache();

        int xCord = (int)eventX;
        int yCord = (int)eventY;


        if (xCord < 0) {
            xCord = 0;
        }else if (xCord > bm.getWidth()-1){
            xCord = bm.getWidth()-1;
        }

        if(yCord < 0){
            yCord = 0;
        }
        else if(yCord > bm.getHeight()-1){
            yCord = bm.getHeight() -1;
        }

        int touchedRGB = bm.getPixel(xCord,yCord);

        int redValue = Color.red(touchedRGB);
        int greenValue = Color.green(touchedRGB);
        int blueValue = Color.blue(touchedRGB);

        if (redValue > 230 && redValue < 240 &&
                greenValue >45 && greenValue < 55 &&
                blueValue > 45 && greenValue < 55){
            detail.setVisibility(View.VISIBLE);
            detailtxt.setText("Telewizor Samsung");
            detailIV.setImageResource(R.drawable.samsung);
            detailtxt.setVisibility(View.VISIBLE);
            detailIV.setVisibility(View.VISIBLE);
            viewBTN.setVisibility(View.VISIBLE);
        }
        else if (redValue > 210 && redValue < 220 && greenValue> 85 && greenValue < 95 && blueValue > 85  && blueValue < 95) {
            detail.setVisibility(View.VISIBLE);
            detailtxt.setText("Szafka RTV");
            detailIV.setImageResource(R.drawable.szafka);
            detailtxt.setVisibility(View.VISIBLE);
            detailIV.setVisibility(View.VISIBLE);
            viewBTN.setVisibility(View.VISIBLE);
        }
        else if (redValue > 190 && redValue < 200 & greenValue > 45 && greenValue < 55 && blueValue>170 && blueValue < 180) {
            detail.setVisibility(View.VISIBLE);
            detailtxt.setText("Talerzyk IKEA");
            detailIV.setImageResource(R.drawable.talerzyk);
            detailtxt.setVisibility(View.VISIBLE);
            detailIV.setVisibility(View.VISIBLE);
            viewBTN.setVisibility(View.VISIBLE);
        }


        Log.d("SINGLETAPUP", "singletapup x " + event.getX() + " Y : " + event.getY()  );
        Log.d("SINGLETAPUP", "R " + redValue +" G " +greenValue + " B " + blueValue);
        return true;

    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        return true;
    }


    public void setuptv() {
        detailIV = findViewById(R.id.detailIV);
        detail = findViewById(R.id.detail);
        detailtxt = findViewById(R.id.detailTXT);
        detail.setVisibility(View.INVISIBLE);
        detailIV.setVisibility(View.INVISIBLE);
        detailtxt.setVisibility(View.INVISIBLE);


        viewBTN = findViewById(R.id.viewBTN);
        viewBTN.setVisibility(View.INVISIBLE);

        viewBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detail.setVisibility(View.INVISIBLE);
                detailIV.setVisibility(View.INVISIBLE);
                detailtxt.setVisibility(View.INVISIBLE);
                viewBTN.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void settouch() {
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }
}
